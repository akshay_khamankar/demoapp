//
//  CustomerTableViewCell.swift
//  DemoApp2020
//
//  Created by Akshay Khamankar on 1/11/21.
//

import UIKit

class CustomerTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var customDescription: UILabel!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    @IBOutlet weak var eyeImage: UIImageView!
    @IBOutlet weak var mailImage: UIImageView!
    @IBOutlet weak var visitorCounter: UILabel!
    @IBOutlet weak var mailCounter: UILabel!
    @IBOutlet weak var unreadVisitorCounter: UILabel!
    @IBOutlet weak var unreadMailCounter: UILabel!
    @IBOutlet weak var unreadEyeView: UIView!
    @IBOutlet weak var unreadMailView: UIView!
    var setData: CustomerDataModel? {
        didSet {
            guard let model = setData else { return }
            self.unreadEyeView.isHidden = model.isUnreadVisitor ?? false ? false : true
            self.unreadMailView.isHidden = model.isUnreadMail ?? false ? false : true
            self.title.text = model.title ?? ""
            self.customDescription.text = model.description ?? ""
            self.image1.image = model.images[0]
            self.image2.image = model.images[1]
            self.image3.image = model.images[2]
            self.image4.image = model.images[3]
            self.showDottedBorderImage(images: [image1, image2, image3, image4])
            self.unreadMailCounter.text = "+3"
            self.unreadVisitorCounter.text = "+2"
        }
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.roundCorners(image: [image1, image2, image3, image4])
        
    }

    func roundCorners(image: [UIImageView]) {
        for someimage in image {
            someimage.layer.cornerRadius = 15
            someimage.clipsToBounds = true
        }
    }
    
    func showDottedBorderImage(images: [UIImageView]) {
        for img in images {
            if img.image == nil {
                img.addLineDashedStroke(pattern: [7, 1], radius: 11, color: UIColor.white.cgColor)
                img.layer.masksToBounds = true
            } else {
                img.layer.borderWidth = 2.0
                img.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


extension UIImageView {
  func addDashedBorder() {
    let color = UIColor.red.cgColor
    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let frameSize = self.frame.size
    let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 3
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [6,1]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
    self.layer.addSublayer(shapeLayer)
    }
}

extension UIImageView {
    @discardableResult
    func addLineDashedStroke(pattern: [NSNumber]?, radius: CGFloat, color: CGColor) -> CALayer {
        let borderLayer = CAShapeLayer()
        borderLayer.strokeColor = color
        borderLayer.lineDashPattern = pattern
        borderLayer.frame = bounds
        borderLayer.fillColor = nil
        borderLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        layer.addSublayer(borderLayer)
        return borderLayer
    }
}
