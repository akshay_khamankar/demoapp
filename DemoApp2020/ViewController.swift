//
//  ViewController.swift
//  DemoApp2020
//
//  Created by Akshay Khamankar on 1/11/21.
//

import UIKit

class ViewController: UIViewController {
    var viewmodel = DummyPageViewModel()
    @IBOutlet weak var dummyTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    func initialSetup() {
        self.viewmodel.setupDatasource()
        self.dummyTableView.tableHeaderView = self.headerView
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewmodel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewmodel.numberOfRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let cellType = self.viewmodel.sections[indexPath.section].rows?[indexPath.row] {
                switch cellType {
                case .CustomerDetails(let model):
                    if let cell = tableView.dequeueReusableCell(withIdentifier: CustomerTableViewCell.identifier) as? CustomerTableViewCell {
                        cell.setData = model
                        return cell
                    }
                }
            }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

