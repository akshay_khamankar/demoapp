//
//  DummyPageViewModel.swift
//  DemoApp2020
//
//  Created by Akshay Khamankar on 1/11/21.
//

import UIKit
// MARK: - SettingAddRecipentModels
enum DummyVCRowType {
    case CustomerDetails(CustomerDataModel)
}
enum DummyVCSectionType: Int {
    case CustomerCell
}

class DummySection {
    var type: DummyVCSectionType?
    var rows: [DummyVCRowType]?
    init(type: DummyVCSectionType?, rows: [DummyVCRowType]?) {
        self.type = type
        self.rows = rows
    }
}

class CustomerDataModel {
    var title: String?
    var description: String?
    var isUnreadVisitor: Bool?
    var isUnreadMail: Bool?
    var numberOfReads: Int?
    var numberOfVisitors: Int?
    var images: [UIImage?]
    var imageHitCounter: Int?
    
    init(title: String, description: String, isUnreadVisitor: Bool, isUnreadMail: Bool, numberOfReads: Int, numberOfVisitors: Int, images: [UIImage?], imageHitCounter: Int) {
        self.title = title
        self.description = description
        self.isUnreadVisitor = isUnreadVisitor
        self.numberOfReads = numberOfReads
        self.numberOfVisitors = numberOfVisitors
        self.images = images
        self.imageHitCounter = imageHitCounter
    }
}

class DummyPageViewModel {
    var sections: [DummySection] = [DummySection]()
    
    // MARK: - Data models for rows
    func numberOfSections() -> Int {
        return sections.count
    }
    func numberOfRows(section: Int) -> Int {
        guard let rowsCount = sections[section].rows?.count else { return 0 }
        return rowsCount
    }
    
    func setupDatasource() {
        let images = [UIImage(named: "swan")!, UIImage(named: "alone")!, UIImage(named: "temple")!, UIImage(named: "teddy")!]
        let images2 = [UIImage(named: "swan")!, UIImage(named: "alone")!, UIImage(named: "temple")!, nil]
        let model1 = CustomerDataModel(title: "Quick Sell", description: "A lot of people have opened your catalogue", isUnreadVisitor: false, isUnreadMail: false, numberOfReads: 34, numberOfVisitors: 54, images: images, imageHitCounter: 22)
        let model2 = CustomerDataModel(title: "Quick Ride", description: "A lot of people have opened your catalogue", isUnreadVisitor: true, isUnreadMail: false, numberOfReads: 34, numberOfVisitors: 54, images: images2, imageHitCounter: 22)
        let section = DummySection(type: .CustomerCell, rows: [.CustomerDetails(model1), .CustomerDetails(model2)])
        sections.append(section)
    }
}
